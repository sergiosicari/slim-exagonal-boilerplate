<?php

use Slim\App;

return function (App $app) {
    $app->get('/', \App\Controller\HomeController::class);
    $app->post('/test', \App\Controller\UserCreateController::class);
};