<?php

$_ENV['SLIM_MODE'] = 'development';

$loader = require __DIR__ . '/../vendor/autoload.php';

use Doctrine\Common\Annotations\AnnotationRegistry;

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

use DI\ContainerBuilder;

// container creation using DI
$containerBuilder = new ContainerBuilder();
$containerBuilder->addDefinitions(__DIR__ . '/container.php');
$container = $containerBuilder->build();
$app = $container->get(App::class);

// routes
(require __DIR__ . '/routes.php')($app);

// middleware 
(require __DIR__ . '/middleware.php')($app);

return $app;