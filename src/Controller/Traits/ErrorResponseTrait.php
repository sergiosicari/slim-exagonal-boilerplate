<?php

namespace App\Controller\Traits;

use App\Dto\Items\ErrorObject;
use App\Dto\Response\ErrorResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

trait ErrorResponseTrait
{
    public function constraintViolationView(ConstraintViolationListInterface $errors): ResponseInterface
    {
        $data = new ErrorResponse();

        foreach ($errors as $ve) {
            $e = new ErrorObject($ve->getMessage());
            $e->setSource($ve->getPropertyPath());
            $e->setTitle('Invalid data');
            $data->add($e);
        }

        $jsonResponse = new JsonResponse($data->all());

        return $jsonResponse;
    }
}
