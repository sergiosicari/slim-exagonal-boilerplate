<?php
declare(strict_types=1);

namespace App\Controller;

use App\Domain\Model\Member\Member;
use JMS\Serializer\SerializerInterface;
use App\Dto\Request\UserCreateRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Controller\Traits\ErrorResponseTrait;
use App\Application\MemberCreator;
use Symfony\Component\Validator\Constraints as Assert;

class UserCreateController
{

    use ErrorResponseTrait;

    private $serializer;
    private $validator;
    private $memberCreator;

    public function __construct(
        SerializerInterface $serializer, 
        ValidatorInterface $validator,
        MemberCreator $memberCreator
    ) {
        $this->serializer = $serializer;  
        $this->validator = $validator;  
        $this->memberCreator = $memberCreator;    
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ) {
        $content = $request->getParsedBody();

        $userDTO = new UserCreateRequest(
            $content['firstName'], 
            $content['lastName'],
            $content['email']
        );
        
        $errors = $this->validator->validate($userDTO);

        if (count($errors) > 0) {
            $errorResponse = $this->constraintViolationView($errors);
            return $errorResponse;
        }
        
        $member = Member::fromArray($content);
        $this->memberCreator->create($member);

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }
}