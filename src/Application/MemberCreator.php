<?php
declare(strict_types=1);

namespace App\Application;

use App\Repository\MemberRepository;
use App\Domain\Model\Member\Member;

class MemberCreator
{

    private $memberRepository;

    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    public function create(Member $member)
    {
        $this->memberRepository->insert($member);
    }

}