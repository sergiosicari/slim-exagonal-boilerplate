<?php

namespace App\Repository;

interface MemberRepository
{
    public function findAll(): array;
    public function insert(): void;
}