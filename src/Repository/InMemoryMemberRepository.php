<?php
declare(strict_types=1);

namespace App\Repository;

class InMemoryMemberRepository implements MemberRepository
{
    public function findAll(): array
    {
        return [];
    }

    public function insert(): void
    {
        
    }
}