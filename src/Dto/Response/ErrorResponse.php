<?php

namespace App\Dto\Response;

use App\Dto\Items\ErrorObject;

class ErrorResponse
{
    private $errors;

    public function __construct(array $errors = [])
    {
        $this->errors = $errors;
    }

    public function add(ErrorObject $error): void
    {
        $this->errors[] = $error;
    }

    public function all(): array
    {
        return $this->errors;
    }

}