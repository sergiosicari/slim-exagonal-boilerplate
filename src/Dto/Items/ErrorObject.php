<?php

namespace App\Dto\Items;

use JsonSerializable;

class ErrorObject implements JsonSerializable
{
    private $details;
    private $title;
    private $source;

    public function __construct(string $details)
    {
        $this->details = $details;
    }

    public function getDetails(): string
    {
        return $this->details;
    }

    public function setDetails(string $details): void
    {
        $this->details = $details;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }


}