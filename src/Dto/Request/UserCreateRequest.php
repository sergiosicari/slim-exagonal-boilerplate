<?php
declare(strict_types=1);

namespace App\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

final class UserCreateRequest
{
    /**
     * @var string
     * @Assert\NotBlank
     */
    private $firstName;
    /**
     * @var string
     * @Assert\NotBlank
     */
    private $lastName;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;

    public function __construct(string $firstName, string $lastName, string $email)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
    }

}
