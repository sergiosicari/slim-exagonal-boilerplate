<?php
declare(strict_types=1);

namespace App\Domain\Model\Member;

use Webmozart\Assert\Assert;

class Member
{
    private $id;
    private $firstName;
    private $lastName;
    private $email;

    private function __construct(
        ?int $id, 
        string $firstName, 
        string $lastName,
        string $email
        )
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
    }

    public static function fromArray(array $data): self
    {
        Assert::notEmpty($data['firstName']);
        Assert::string($data['firstName']);
        Assert::notEmpty($data['lastName']);
        Assert::string($data['lastName']);
        Assert::notEmpty($data['email']);
        Assert::string($data['email']);
        Assert::email($data['email']);
        return new self(
            null,
            $data['firstName'],
            $data['lastName'],
            $data['email']
        );
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
    
    public function getEmail(): string
    {
        return $this->email;
    }


}